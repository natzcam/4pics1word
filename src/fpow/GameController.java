/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpow;

import fpow.config.Game;
import fpow.config.GameData;
import fpow.control.ClueButton;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

/**
 *
 * @author nathaniel.a.camomot
 */
public class GameController implements Initializable {

    private final ConfigManager configManager = new ConfigManager();

    @FXML
    private GridPane picsGrid;
    @FXML
    private HBox wordBox;
    @FXML
    private GridPane clueGrid;
    @FXML
    private ImageView imgView1;
    @FXML
    private ImageView imgView2;
    @FXML
    private ImageView imgView3;
    @FXML
    private ImageView imgView4;

    private GameData gameData;

    private Game currentGame = null;
    private int gameIndex = -1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        configManager.load(getClass().getResourceAsStream("config/config.json"));
        gameData = configManager.getGameData();

        gotoNextGame();
    }
    
    private void gotoNextGame() {
        gameIndex++;
        if (gameIndex < gameData.getGames().size()) {
            currentGame = gameData.getGames().get(gameIndex);
            initGame();
        } else {
            finished();      
        }
    }

    private Image createImage(String path) {
        return new Image(path, 50, 50, false, false);
    }

    private ClueButton createClueButton(char c, int col, int row) {
        ClueButton btn = new ClueButton(c, col, row);
        btn.setFont(new Font("System Bold", 24));
        btn.setPrefHeight(60);
        btn.setPrefWidth(60);

        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                if (btn.isAnswered()) {
                    wordBox.getChildren().remove(btn);
                    clueGrid.add(btn, btn.getColumn(), btn.getRow());
                    btn.setAnswered(false);
                } else {
                    if (wordBox.getChildren().size() < currentGame.getAnswer().length()) {
                        clueGrid.getChildren().remove(btn);
                        wordBox.getChildren().add(btn);
                        btn.setAnswered(true);
                        checkAnswer();
                    }
                }
            }
        });
        return btn;
    }

    private void initGame() {
        List<String> pics = currentGame.getPics();
        imgView1.setImage(createImage(pics.get(0)));
        imgView2.setImage(createImage(pics.get(1)));
        imgView3.setImage(createImage(pics.get(2)));
        imgView4.setImage(createImage(pics.get(3)));

        wordBox.getChildren().clear();
        clueGrid.getChildren().clear();

        String clues = currentGame.getClues();
        final int mid = clues.length() / 2;
        String part1 = clues.substring(0, mid);
        String part2 = clues.substring(mid);

        for (int i = 0; i < part1.length(); i++) {
            char c = part1.charAt(i);
            Button b = createClueButton(c, i, 0);
            clueGrid.add(b, i, 0);
        }

        for (int i = 0; i < part2.length(); i++) {
            char c = part2.charAt(i);
            Button b = createClueButton(c, i, 1);
            clueGrid.add(b, i, 1);
        }
    }

    private void checkAnswer() {
        String str = "";
        for (Node ch : wordBox.getChildren()) {
            ClueButton cb = (ClueButton) ch;
            str += cb.getText();
        }

        if (str.equals(currentGame.getAnswer())) {
            yehey();
        }
    }

    private void yehey() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Yehey!");
        alert.setHeaderText("Correct! You got the correct word!");
        alert.setContentText(null);
        alert.showAndWait();

        gotoNextGame();
    }

    private void finished() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Wow!");
        alert.setHeaderText("You have finished the entire game! Congratz!");
        alert.setContentText(null);
        alert.showAndWait();

        Platform.exit();
        System.exit(0);
    }
}
