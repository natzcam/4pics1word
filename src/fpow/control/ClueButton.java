/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpow.control;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author nathaniel.a.camomot
 */
public class ClueButton extends Button {

    private final int column;
    private final int row;
    private boolean answered = false;

    public ClueButton(char c, int column, int row) {
        super(new String(new char[]{c}));
        this.column = column;
        this.row = row;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
