/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpow.config;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nathaniel.a.camomot
 */
public class Game {

    private List<String> pics = new ArrayList<String>();
    private String answer;
    private String clues;

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getClues() {
        return clues;
    }

    public void setClues(String clues) {
        this.clues = clues;
    }
    
    
}
