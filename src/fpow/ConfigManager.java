/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpow;

import com.google.gson.Gson;
import fpow.config.GameData;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author nathaniel.a.camomot
 */
public class ConfigManager {
    private GameData gameData;
    private final Gson gson = new Gson();
    
    public void load(InputStream inputStream){
        gameData = gson.fromJson(new InputStreamReader(inputStream), GameData.class);
        System.out.println(gameData);
    }

    public GameData getGameData() {
        return gameData;
    }
}
